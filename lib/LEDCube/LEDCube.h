#include <Arduino.h>
#include <SPI.h>

#define CUBESIZE 8 //ToDo: move this parameter to main.cpp ?? 

#define XAXIS 0
#define YAXIS 1
#define ZAXIS 2

#define POS_X 0
#define NEG_X 1
#define POS_Z 2
#define NEG_Z 3
#define POS_Y 4
#define NEG_Y 5

#define TRANSPOSE 1 // 0 = don't transpose, 1 = transpose

class Cube
{
public:
  static uint8_t effectcube[CUBESIZE][CUBESIZE];
  static uint8_t physcube[CUBESIZE][CUBESIZE];
  bool updateCube;

  void clear();
  void lightUp();
  void transpose8bit(); // transpose effects to physical cube 
  void render();

  void setVoxel(uint8_t x, uint8_t y, uint8_t z);
  void clearVoxel(uint8_t x, uint8_t y, uint8_t z);
  bool getVoxel(uint8_t x, uint8_t y, uint8_t z);

  void shift(uint8_t dir);
  
  // remove?
  void text(char string[], uint8_t len);
};