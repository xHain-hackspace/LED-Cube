#include <LEDCube.h>

uint8_t Cube::effectcube[CUBESIZE][CUBESIZE];
uint8_t Cube::physcube[CUBESIZE][CUBESIZE];

void Cube::clear()
{
    for (uint8_t j = 0; j < CUBESIZE; j++) {
        for (uint8_t k = 0; k < CUBESIZE; k++) {
         effectcube[j][k] = 0x0;
         physcube[j][k] = 0x0;
        }
    }
};

void Cube::lightUp() {
  for (uint8_t i = 0; i < CUBESIZE; i++) {
    for (uint8_t j = 0; j < CUBESIZE; j++) {
      effectcube[i][j] = 0xFF;
    }
  }
};

void Cube::transpose8bit() {
  // rotate 90°, save rotated data temporarily in physcube
  for (uint8_t i = 0; i < CUBESIZE; i++){ // verical layer (ground pins) 
    for(uint8_t j=0;j<CUBESIZE;j++) {
      uint8_t x = effectcube[i][j];
      for (uint8_t k=0;k<CUBESIZE;k++){
        physcube[i][k]=physcube[i][k]<<1;
        if (x & 1) {
            physcube[i][k]+=1;
        }
        x = x>>1;
      }
    }
  }
  //copy from physcube back into effectcube
  for (uint8_t i = 0; i < CUBESIZE; i++){ // verical layer (ground pins) 
    for(uint8_t j=0;j<CUBESIZE;j++) {
        effectcube[i][j]=physcube[i][j];
    }
  }
};

void Cube::render() {
  for (uint8_t i = 0; i < CUBESIZE; i++) { // iterate over 64 vcc led-pins
    digitalWrite(SS, LOW); // latch pin off (take in data, don't write yet)
    SPI.transfer(0x01 << i); // activate one ground layer 
    for (uint8_t j = 0; j < CUBESIZE; j++) {
      SPI.transfer(effectcube[i][j]); // set all rows/columns to the byte written in effectcube
    }
    digitalWrite(SS, HIGH); // latch pin on (write now)
  }
};

void Cube::setVoxel(uint8_t x, uint8_t y, uint8_t z) {
  effectcube[(CUBESIZE-1) - y][(CUBESIZE-1) - z] |= (0x01 << x);
};

void Cube::clearVoxel(uint8_t x, uint8_t y, uint8_t z) {
  effectcube[(CUBESIZE-1) - y][(CUBESIZE-1) - z] ^= (0x01 << x);
};

bool Cube::getVoxel(uint8_t x, uint8_t y, uint8_t z) {
  return (effectcube[(CUBESIZE-1) - y][(CUBESIZE-1) - z] & (0x01 << x)) == (0x01 << x);
};

void Cube::shift(uint8_t dir) {
  if (dir == POS_X) {
    for (uint8_t y = 0; y < CUBESIZE; y++) {
      for (uint8_t z = 0; z < CUBESIZE; z++) {
        effectcube[y][z] = effectcube[y][z] << 1;
      }
    }
  } else if (dir == NEG_X) {
    for (uint8_t y = 0; y < CUBESIZE; y++) {
      for (uint8_t z = 0; z < CUBESIZE; z++) {
        effectcube[y][z] = effectcube[y][z] >> 1;
      }
    }
  } else if (dir == POS_Y) {
    for (uint8_t y = 1; y < CUBESIZE; y++) {
      for (uint8_t z = 0; z < CUBESIZE; z++) {
        effectcube[y - 1][z] = effectcube[y][z];
      }
    }
    for (uint8_t i = 0; i < CUBESIZE; i++) {
      effectcube[(CUBESIZE-1)][i] = 0;
    }
  } else if (dir == NEG_Y) {
    for (uint8_t y = (CUBESIZE-1); y > 0; y--) {
      for (uint8_t z = 0; z < CUBESIZE; z++) {
        effectcube[y][z] = effectcube[y - 1][z];
      }
    }
    for (uint8_t i = 0; i < CUBESIZE; i++) {
      effectcube[0][i] = 0;
    }
  } else if (dir == POS_Z) {
    for (uint8_t y = 0; y < CUBESIZE; y++) {
      for (uint8_t z = 1; z < CUBESIZE; z++) {
        effectcube[y][z - 1] = effectcube[y][z];
      }
    }
    for (uint8_t i = 0; i < CUBESIZE; i++) {
      effectcube[i][CUBESIZE-1] = 0;
    }
  } else if (dir == NEG_Z) {
    for (uint8_t y = 0; y < CUBESIZE; y++) {
      for (uint8_t z = (CUBESIZE-1); z > 0; z--) {
        effectcube[y][z] = effectcube[y][z - 1];
      }
    }
    for (uint8_t i = 0; i < CUBESIZE; i++) {
      effectcube[i][0] = 0;
    }
  }
};
