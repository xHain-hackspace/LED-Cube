// #include <LEDCube.h>
#include <Effects.h>

#define RAIN_TIME 250
#define PLANE_BOING_TIME 2500 //original 500, speed/duration of shifting planes
#define SEND_VOXELS_TIME 140
#define WOOP_WOOP_TIME 350 //original 800
#define CUBE_JUMP_TIME 200 //original 500
#define GLOW_TIME 8
#define TEXT_TIME 1000 // original 500
#define CLOCK_TIME 500

void initAll();
void clearCube();
void renderCube();
void updateCube(bool loading);

uint16_t renderRain(uint16_t timer);
uint16_t renderPlaneBoing(uint16_t timer);
uint16_t renderSendVoxels(uint16_t timer);
uint16_t renderWoopWoop(uint16_t timer);
uint16_t renderCubeJump(uint16_t timer);
uint16_t renderGlow(uint16_t timer);
uint16_t renderAllOn(uint16_t timer);
uint16_t renderText(uint16_t timer);

void renderTestFunct(); // REMOVE test function