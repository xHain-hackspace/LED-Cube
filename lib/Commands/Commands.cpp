#include <Commands.h>

Cube cube;
Cube * cubePointer;
Plane plane;
Voxels voxels;
Woop woop;
Text text;


// functions
void initAll(){
    cubePointer = &cube;
    cube.updateCube = true;
    cube.clear();
    plane.init();
    voxels.init();
    woop.init();
    text.init(cubePointer);
};

void clearCube(){
    cube.clear();
};

void renderCube(){
    cube.render();
};

void updateCube(bool loading){
    cube.updateCube = loading;
};

void renderTestFunct(){ // REMOVE test function
    testFunct(cubePointer);

};


// effects
uint16_t renderRain(uint16_t timer){
    if (cube.updateCube) {
        cube.clear();
        cube.updateCube = false;
    }
    timer++;
    if (timer > RAIN_TIME) {
        timer = 0;
        rain(cubePointer);
    }
    return timer;
};

uint16_t renderPlaneBoing(uint16_t timer) {
  if (cube.updateCube) {
    cube.clear();
    uint8_t axis = random(0, 3);
    plane.position = random(0, 2) * (CUBESIZE-1);
    plane.setPlane(cubePointer, axis);
    plane.setDirection(axis);
    timer = 0;
    plane.looped = false;
    cube.updateCube = false;
  }

  timer++;
  if (timer > PLANE_BOING_TIME) {
    timer = 0;
    cube.shift(plane.direction);
    plane.planeBoing(cubePointer);
  }
  return timer;
};

uint16_t renderSendVoxels(uint16_t timer) {
    if (cube.updateCube) {
        cube.clear();
        for (uint8_t x = 0; x < CUBESIZE; x++) {
        for (uint8_t z = 0; z < CUBESIZE; z++) {
            cube.setVoxel(x, random(0, 2) * (CUBESIZE-1), z);
        }
        }
        cube.updateCube = false;
    }

    timer++;
    if (timer > SEND_VOXELS_TIME) {
        timer = 0;
        voxels.sendVoxel(cubePointer);
    }
    return timer;
};

uint16_t renderWoopWoop(uint16_t timer) {
    if (cube.updateCube) {
        cube.clear();
        woop.cubeSize = 2;
        woop.cubeExpanding = true;
        cube.updateCube = false;
    }

    timer++;
    if (timer > WOOP_WOOP_TIME) {
        timer = 0;
        woop.doWoop(cubePointer);
    }
    return timer;
};

uint16_t renderCubeJump(uint16_t timer) {
    if (cube.updateCube) {
        cube.clear();
        woop.xPos = random(0, 2) * (CUBESIZE-1);
        woop.yPos = random(0, 2) * (CUBESIZE-1);
        woop.zPos = random(0, 2) * (CUBESIZE-1);
        woop.cubeSize = CUBESIZE;
        woop.cubeExpanding = false;
        cube.updateCube = false;
    }

    timer++;
    if (timer > CUBE_JUMP_TIME) {
        timer = 0;
        cube.clear();
        woop.doJump(cubePointer);
    }
    return timer;
};

uint16_t renderGlow(uint16_t timer) {
  if (cube.updateCube) {
    cube.clear();
    voxels.glowCount = 0;
    voxels.glowing = true;
    cube.updateCube = false;
  };

  timer++;
  if (timer > GLOW_TIME) {
    timer = 0;
    voxels.doGlow(cubePointer);
  };
  return timer;
};

uint16_t renderAllOn(uint16_t timer) {
  if (cube.updateCube) {
    cube.clear();
    cube.lightUp();
    cube.updateCube = false;
  }
  return timer;
};

uint16_t renderText(uint16_t timer) {
    if (cube.updateCube) {
        cube.clear();
        text.charPosition = -1;
        text.charCounter = 0;
        cube.updateCube = false;
    }
    timer++;
    if (timer > TEXT_TIME) {
        timer = 0;
        //text.showText(cubePointer, "0123456789", 10); // outdated
        //text.showText(cubePointer, NUMBERS8; //10 numbers
        text.showText(cubePointer, XHAIN8);
    };
    return timer;
};
