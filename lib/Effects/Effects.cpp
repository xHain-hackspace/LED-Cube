#include <Effects.h>
#include <Alphabet8x8.h>

// plane boing
uint8_t Plane::position;
uint8_t Plane::direction;
bool Plane::looped;
uint8_t Plane::currentEffect;

// send voxels
uint8_t Voxels::selX;
uint8_t Voxels::selY;
uint8_t Voxels::selZ;
uint8_t Voxels::sendDirection;
bool Voxels::sending;

// woop woop
uint8_t Woop::cubeSize;
bool Woop::cubeExpanding;

// jumping cube
uint8_t Woop::xPos;
uint8_t Woop::yPos;
uint8_t Woop::zPos;

// glow
uint16_t Voxels::glowCount;
bool Voxels::glowing;

// text
uint8_t Text::charCounter;
uint8_t Text::charPosition;
uint8_t Text::textxhain[5][8];


//////////////// EFFECTS //////////////

// TEST EFFECT
void testFunct(Cube * cubePointer) { // REMOVE test function
  cubePointer->effectcube[0][0] = 0x3C;
};

// RAIN EFFECT
void rain(Cube * rainCube) {
  rainCube->shift(NEG_Y);

  uint8_t numDrops = random(0, 5);
  for (uint8_t i = 0; i < numDrops; i++) {
    rainCube->effectcube[0][CUBESIZE-1 - random(0, 8)] |= (0x01 << random(0, CUBESIZE));
  }
};

// PLANE BOING EFFECT
void Plane::init(){
  position = 0;
  direction = 0;
  looped = false;
  currentEffect = RAIN;
};

void Plane::setPlane(Cube * planeCube, uint8_t axis) {
  for (uint8_t j = 0; j < CUBESIZE; j++) {
    for (uint8_t k = 0; k < CUBESIZE; k++) {
      if (axis == XAXIS) {
        planeCube->setVoxel(position, j, k);
      } else if (axis == YAXIS) {
        planeCube->setVoxel(j, position, k);
      } else if (axis == ZAXIS) {
        planeCube->setVoxel(j, k, position);
      }
    }
  }
};

 void Plane::setDirection(uint8_t axis){
    if (axis == XAXIS) {
        if (position == 0) {
        direction = POS_X;
        } else {
        direction = NEG_X;
        }
    } else if (axis == YAXIS) {
        if (position == 0) {
        direction = POS_Y;
        } else {
        direction = NEG_Y;
        }
    } else if (axis == ZAXIS) {
        if (position == 0) {
        direction = POS_Z;
        } else {
        direction = NEG_Z;
        }
    }
};

void Plane::planeBoing(Cube * planeCube)
{
  if (direction % 2 == 0) {
    position++;
    if (position == (CUBESIZE-1)) {
      if (looped) {
        planeCube->updateCube = true;
      } else {
        direction++;
        looped = true;
      }
    }
  } else {
    position--;
    if (position == 0) {
      if (looped) {
        planeCube->updateCube = true;
      } else {
        direction--;
        looped = true;
      }
    }
  }
}

// SEND VOXELS EFFECT
void Voxels::init(){
  selX = 0;
  selY = 0;
  selZ = 0;
  sendDirection = 0;
  sending = false;
  glowCount = 0;
  glowing = false;
};

void Voxels::sendVoxel(Cube * voxelCube){
    if (!sending) {
        selX = random(0, CUBESIZE);
        selZ = random(0, CUBESIZE);
        if (voxelCube->getVoxel(selX, 0, selZ)) {
            selY = 0;
            sendDirection = POS_Y;
        } else if (voxelCube->getVoxel(selX, (CUBESIZE-1), selZ)) {
            selY = 7;
            sendDirection = NEG_Y;
        }
        sending = true;
    } else {
        if (sendDirection == POS_Y) {
            selY++;
            voxelCube->setVoxel(selX, selY, selZ);
            voxelCube->clearVoxel(selX, selY - 1, selZ);
            if (selY == (CUBESIZE-1)) {
                sending = false;
            }
        } else {
            selY--;
            voxelCube->setVoxel(selX, selY, selZ);
            voxelCube->clearVoxel(selX, selY + 1, selZ);
            if (selY == 0) {
                sending = false;
            }
        }
    }
};

// WOOP WOOP EFFECT
void Woop::init(){
  cubeSize = 0;
  cubeExpanding = true;
};

void Woop::drawCube(Cube * woopCube, uint8_t x, uint8_t y, uint8_t z, uint8_t s) {
  for (uint8_t i = 0; i < s; i++) {
    woopCube->setVoxel(x, y + i, z);
    woopCube->setVoxel(x + i, y, z);
    woopCube->setVoxel(x, y, z + i);
    woopCube->setVoxel(x + s - 1, y + i, z + s - 1);
    woopCube->setVoxel(x + i, y + s - 1, z + s - 1);
    woopCube->setVoxel(x + s - 1, y + s - 1, z + i);
    woopCube->setVoxel(x + s - 1, y + i, z);
    woopCube->setVoxel(x, y + i, z + s - 1);
    woopCube->setVoxel(x + i, y + s - 1, z);
    woopCube->setVoxel(x + i, y, z + s - 1);
    woopCube->setVoxel(x + s - 1, y, z + i);
    woopCube->setVoxel(x, y + s - 1, z + i);
  }
};

void Woop::doWoop(Cube * woopCube){
  if (cubeExpanding) {
      cubeSize += 2;
      if (cubeSize == CUBESIZE) {
          cubeExpanding = false;
      }
  } else {
      cubeSize -= 2;
      if (cubeSize == 2) {
          cubeExpanding = true;
      }
  }
  woopCube->clear();
  drawCube(woopCube, 4 - cubeSize / 2, 4 - cubeSize / 2, 4 - cubeSize / 2, cubeSize); // might need to be edited for different cube sizes??? 
};

// CUBE JUMP EFFECT
void Woop::doJump(Cube * jumpCube){
  if (xPos == 0 && yPos == 0 && zPos == 0) {
    drawCube(jumpCube, xPos, yPos, zPos, cubeSize);
  } else if (xPos == 7 && yPos == (CUBESIZE-1) && zPos == (CUBESIZE-1)) {
    drawCube(jumpCube, xPos + 1 - cubeSize, yPos + 1 - cubeSize, zPos + 1 - cubeSize, cubeSize);
  } else if (xPos == 7 && yPos == 0 && zPos == 0) {
    drawCube(jumpCube, xPos + 1 - cubeSize, yPos, zPos, cubeSize);
  } else if (xPos == 0 && yPos == (CUBESIZE-1) && zPos == 0) {
    drawCube(jumpCube, xPos, yPos + 1 - cubeSize, zPos, cubeSize);
  } else if (xPos == 0 && yPos == 0 && zPos == (CUBESIZE-1)) {
    drawCube(jumpCube, xPos, yPos, zPos + 1 - cubeSize, cubeSize);
  } else if (xPos == 7 && yPos == (CUBESIZE-1) && zPos == 0) {
    drawCube(jumpCube, xPos + 1 - cubeSize, yPos + 1 - cubeSize, zPos, cubeSize);
  } else if (xPos == 0 && yPos == (CUBESIZE-1) && zPos == (CUBESIZE-1)) {
    drawCube(jumpCube, xPos, yPos + 1 - cubeSize, zPos + 1 - cubeSize, cubeSize);
  } else if (xPos == 7 && yPos == 0 && zPos == (CUBESIZE-1)) {
    drawCube(jumpCube, xPos + 1 - cubeSize, yPos, zPos + 1 - cubeSize, cubeSize);
  }
  if (cubeExpanding) {
    cubeSize++;
    if (cubeSize >= CUBESIZE) {
      cubeExpanding = false;
      xPos = random(0, 2) * (CUBESIZE-1);
      yPos = random(0, 2) * (CUBESIZE-1);
      zPos = random(0, 2) * (CUBESIZE-1);
    }
  } else {
    cubeSize--;
    if (cubeSize <= 1) {
      cubeExpanding = true;
    }
  }
};

// GLOW EFFECT
void Voxels::doGlow(Cube * glowCube){
  if (glowing) {
    if (glowCount < 448) { // hardcoded number!
      do {
        selX = random(0, CUBESIZE);
        selY = random(0, CUBESIZE);
        selZ = random(0, CUBESIZE);
      } while (glowCube->getVoxel(selX, selY, selZ));
      glowCube->setVoxel(selX, selY, selZ);
      glowCount++;
    } else if (glowCount < 512) { // 512 LEDs bei 8x8 Cube!!! --> anpassen für andere Größen!
      glowCube->lightUp();
      glowCount++;
    } else {
      glowing = false;
      glowCount = 0;
    }
  } else {
    if (glowCount < 448) { // again, hardcoded number !!! 
      do {
        selX = random(0, CUBESIZE);
        selY = random(0, CUBESIZE);
        selZ = random(0, CUBESIZE);
      } while (!glowCube->getVoxel(selX, selY, selZ));
      glowCube->clearVoxel(selX, selY, selZ);
      glowCount++;
    } else {
      glowCube->clear();
      glowing = true;
      glowCount = 0;
    }
  }
};

// TEXT EFFECT
void Text::init(Cube * textCube)
{
  charCounter = 0;
  charPosition = 0;
  
  for (uint8_t i = 0; i<8; i++){
    textxhain[0][i] = x_upper[i]; 
    textxhain[1][i] = h_upper[i]; 
    textxhain[2][i] = a_upper[i];
    textxhain[3][i] = i_upper[i];
    textxhain[4][i] = n_upper[i]; 
  };
};

void Text::showText(Cube * textCube, int mode){
  if (TRANSPOSE == 0) {
    textCube->shift(NEG_Z);
  }
  else if (TRANSPOSE == 1) {
    textCube->shift(NEG_X);
  }
  charPosition++;
  uint8_t len = 10;

  switch (mode) {
    case NUMBERS8: len = 10; break; 
    case XHAIN8: len = 5; break;
    default: len = 10;  
  }

  if (charPosition == (CUBESIZE-1)) {
    charCounter++;
    if (charCounter > len - 1) {
        charCounter = 0;
    }
    charPosition = 0;
  }

  if (charPosition == 0) {
    textCube->clear();
    for (uint8_t i = 0; i < CUBESIZE; i++) {
      switch (mode) {
        case NUMBERS8: textCube->effectcube[i][0] = numbers8x8[charCounter][i]; break; 
        case XHAIN8: textCube->effectcube[i][0] = textxhain[charCounter][i]; break;
        default: textCube->effectcube[i][0] = numbers8x8[charCounter][i]; 
      }
    }
    if (TRANSPOSE >= 1){
      textCube->transpose8bit();
    }
  }
};