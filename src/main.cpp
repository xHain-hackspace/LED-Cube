// Source: https://gitlab.com/xHain-hackspace/LED-Cube
// reworked version of: https://github.com/itsharryle/LED_CUBE
#include <Arduino.h>
#include <SPI.h>
#include <Commands.h>

// The Arduino Uno/Nano is connected to the first shift register (SN74HC595N) as follows:
// Arduino: D11 MOSI <--> P14 SER shift register (data)
// Arduino: D13 SCK  <--> P11 SRCLK shift register (data clock)
// Arduino: D10 SS   <--> P12 RCLK shift register (latch pin i.e. write out value now)
#define BUTTON_PIN 8
#define RED_LED 5
#define GREEN_LED 7

// User Settings Paramenters:
bool autoRotate = true; //automatically change effect mode after timeout
uint64_t lastEffectChange = 0;
uint32_t effectDuration = 10000; // original 5000 

// other parameters
uint8_t currentEffect = 0;
uint16_t timer = 0;
uint16_t randomTimer;


void setup() {
  initAll();

  // randomTimer = 0;
  // currentEffect = 0;

  SPI.begin();
  SPI.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));

  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);

  randomSeed(analogRead(0));
  digitalWrite(GREEN_LED, HIGH);

  Serial.begin(9600);
  Serial.println("hello");
}

void loop() {
  uint32_t currentTime = millis();  //millis() returns uint32 and overflows after about 50days. Effects may stop working as expected after overlow. 
  randomTimer++;

  if (digitalRead(BUTTON_PIN) == LOW || (currentTime - lastEffectChange >= effectDuration && autoRotate)) {
    lastEffectChange = millis();
    clearCube(); 
    updateCube(true);
    timer = 0;
    currentEffect++;
    if (currentEffect >= TOTAL_EFFECTS) {
      currentEffect = 0;
    }
    randomSeed(randomTimer);
    randomTimer = 0;
    digitalWrite(RED_LED, HIGH);
    digitalWrite(GREEN_LED, LOW);
    delay(500);
    digitalWrite(RED_LED, LOW);
    digitalWrite(GREEN_LED, HIGH);
  }

  switch (currentEffect) {
    case RAIN: timer = renderRain(timer); break;
    case PLANE_BOING: timer = renderPlaneBoing(timer); break;
    case SEND_VOXELS: timer = renderSendVoxels(timer); break;
    case WOOP_WOOP: timer = renderWoopWoop(timer); break;
    case CUBE_JUMP: timer = renderCubeJump(timer); break;
    case GLOW: timer = renderGlow(timer); break;
    case LIT: timer = renderAllOn(timer); break;
    case TEXT: timer = renderText(timer); break;

    default: timer = renderText(timer);
  }

  renderCube();
}