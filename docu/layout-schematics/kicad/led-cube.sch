EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "8x8x8 LED-Cube"
Date "2020-10-15"
Rev ""
Comp "https://gitlab.com/xHain-hackspace/LED-Cube"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L led-cube-rescue:CP-Device C1
U 1 1 5F893D8B
P 1450 750
F 0 "C1" V 1195 750 50  0001 C CNN
F 1 "CP" V 1287 750 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 1488 600 50  0001 C CNN
F 3 "~" H 1450 750 50  0001 C CNN
	1    1450 750 
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:CP-Device C2
U 1 1 5F8952FA
P 5550 6550
F 0 "C2" H 5668 6596 50  0001 L CNN
F 1 "CP" V 5400 6550 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.80mm" H 5588 6400 50  0001 C CNN
F 3 "~" H 5550 6550 50  0001 C CNN
	1    5550 6550
	0    -1   -1   0   
$EndComp
Connection ~ 3000 5450
Wire Wire Line
	3000 5450 3000 6100
Connection ~ 2600 5800
Wire Wire Line
	2600 5800 2600 6100
Wire Wire Line
	3200 5800 2600 5800
Wire Wire Line
	3200 6100 3200 5800
Wire Wire Line
	2800 6400 3400 6400
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z4
U 1 1 5F8F098A
P 3400 6200
F 0 "Q2_Z4" V 3728 6200 50  0000 C CNN
F 1 "PN2222A" V 3637 6200 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3600 6125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 3400 6200 50  0001 L CNN
	1    3400 6200
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z4
U 1 1 5F8F0984
P 2800 6200
F 0 "Q1_Z4" V 3128 6200 50  0000 C CNN
F 1 "PN2222A" V 3037 6200 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3000 6125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 2800 6200 50  0001 L CNN
	1    2800 6200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 4800 3000 5450
Connection ~ 2600 5150
Wire Wire Line
	2600 5150 2500 5150
Wire Wire Line
	2600 5150 2600 5450
Wire Wire Line
	3200 5150 2600 5150
Wire Wire Line
	3200 5450 3200 5150
Wire Wire Line
	2800 5750 3400 5750
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z3
U 1 1 5F8E3B2F
P 3400 5550
F 0 "Q2_Z3" V 3728 5550 50  0000 C CNN
F 1 "PN2222A" V 3637 5550 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3600 5475 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 3400 5550 50  0001 L CNN
	1    3400 5550
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z3
U 1 1 5F8E3B29
P 2800 5550
F 0 "Q1_Z3" V 3128 5550 50  0000 C CNN
F 1 "PN2222A" V 3037 5550 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3000 5475 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 2800 5550 50  0001 L CNN
	1    2800 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 4150 3000 4800
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z1
U 1 1 5F8CE043
P 3400 4250
F 0 "Q2_Z1" V 3728 4250 50  0000 C CNN
F 1 "PN2222A" V 3637 4250 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3600 4175 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 3400 4250 50  0001 L CNN
	1    3400 4250
	0    -1   -1   0   
$EndComp
Connection ~ 3000 4800
Connection ~ 2600 4500
Wire Wire Line
	2600 4500 2600 4800
Wire Wire Line
	3200 4500 2600 4500
Wire Wire Line
	3200 4800 3200 4500
Wire Wire Line
	2800 5100 3400 5100
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z2
U 1 1 5F8DCB69
P 3400 4900
F 0 "Q2_Z2" V 3728 4900 50  0000 C CNN
F 1 "PN2222A" V 3637 4900 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3600 4825 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 3400 4900 50  0001 L CNN
	1    3400 4900
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z2
U 1 1 5F8DCB63
P 2800 4900
F 0 "Q1_Z2" V 3128 4900 50  0000 C CNN
F 1 "PN2222A" V 3037 4900 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3000 4825 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 2800 4900 50  0001 L CNN
	1    2800 4900
	0    -1   -1   0   
$EndComp
Connection ~ 3000 4150
Connection ~ 2600 3850
Wire Wire Line
	2600 3850 2600 4150
Wire Wire Line
	3200 3850 2600 3850
Wire Wire Line
	3200 4150 3200 3850
Wire Wire Line
	2800 4450 3400 4450
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z1
U 1 1 5F88545E
P 2800 4250
F 0 "Q1_Z1" V 3128 4250 50  0000 C CNN
F 1 "PN2222A" V 3037 4250 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 3000 4175 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 2800 4250 50  0001 L CNN
	1    2800 4250
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:VCC-power #PWR0101
U 1 1 5F90ED06
P 1900 700
F 0 "#PWR0101" H 1900 550 50  0001 C CNN
F 1 "VCC" H 1915 873 50  0000 C CNN
F 2 "" H 1900 700 50  0001 C CNN
F 3 "" H 1900 700 50  0001 C CNN
	1    1900 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5450 3600 6100
Wire Wire Line
	3600 5450 3600 4800
Connection ~ 3600 5450
Wire Wire Line
	3600 4800 3600 4150
Connection ~ 3600 4800
Connection ~ 3600 4150
Wire Wire Line
	1900 700  1900 750 
$Comp
L led-cube-rescue:GNDREF-power #PWR0102
U 1 1 5F91034B
P 850 850
F 0 "#PWR0102" H 850 600 50  0001 C CNN
F 1 "GNDREF" H 855 677 50  0000 C CNN
F 2 "" H 850 850 50  0001 C CNN
F 3 "" H 850 850 50  0001 C CNN
	1    850  850 
	1    0    0    -1  
$EndComp
Connection ~ 3400 4450
Wire Wire Line
	3750 6950 3750 5750
Wire Wire Line
	3700 7050 3700 6400
Connection ~ 3400 6400
$Comp
L led-cube-rescue:74AHC595-74xx U9_GND1
U 1 1 5F880FA1
P 4400 7050
F 0 "U9_GND1" H 4200 6400 50  0000 C CNN
F 1 "74AHC595" V 4400 6900 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 4400 7050 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 4400 7050 50  0001 C CNN
	1    4400 7050
	-1   0    0    1   
$EndComp
Connection ~ 3400 5100
Wire Wire Line
	3400 5750 3750 5750
Connection ~ 3400 5750
Wire Wire Line
	1600 750  1900 750 
Connection ~ 1900 750 
Wire Wire Line
	4800 7450 6100 7450
$Comp
L led-cube-rescue:74AHC595-74xx U8_X8
U 1 1 5F87951C
P 5800 1350
F 0 "U8_X8" H 5950 1900 50  0000 C CNN
F 1 "74AHC595" V 5800 1200 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 5800 1350 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 5800 1350 50  0001 C CNN
	1    5800 1350
	1    0    0    -1  
$EndComp
$Comp
L led-cube-rescue:R-Device R64
U 1 1 5F8964F1
P 6350 950
F 0 "R64" V 6143 950 50  0000 C CNN
F 1 "R100" V 6234 950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 950 50  0001 C CNN
F 3 "~" H 6350 950 50  0001 C CNN
	1    6350 950 
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R63
U 1 1 5F899BB9
P 6600 1050
F 0 "R63" V 6393 1050 50  0000 C CNN
F 1 "R100" V 6485 1050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 1050 50  0001 C CNN
F 3 "~" H 6600 1050 50  0001 C CNN
	1    6600 1050
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R62
U 1 1 5F89CFA8
P 6850 1150
F 0 "R62" V 6643 1150 50  0000 C CNN
F 1 "R100" V 6735 1150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 1150 50  0001 C CNN
F 3 "~" H 6850 1150 50  0001 C CNN
	1    6850 1150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R61
U 1 1 5F89DE14
P 7100 1250
F 0 "R61" V 6893 1250 50  0000 C CNN
F 1 "R100" V 6985 1250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 1250 50  0001 C CNN
F 3 "~" H 7100 1250 50  0001 C CNN
	1    7100 1250
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R60
U 1 1 5F8A0FA9
P 6350 1350
F 0 "R60" V 6143 1350 50  0000 C CNN
F 1 "R100" V 6235 1350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 1350 50  0001 C CNN
F 3 "~" H 6350 1350 50  0001 C CNN
	1    6350 1350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R59
U 1 1 5F8A0FAF
P 6600 1450
F 0 "R59" V 6393 1450 50  0000 C CNN
F 1 "R100" V 6485 1450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 1450 50  0001 C CNN
F 3 "~" H 6600 1450 50  0001 C CNN
	1    6600 1450
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R58
U 1 1 5F8A0FB6
P 6850 1550
F 0 "R58" V 6643 1550 50  0000 C CNN
F 1 "R100" V 6735 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 1550 50  0001 C CNN
F 3 "~" H 6850 1550 50  0001 C CNN
	1    6850 1550
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R57
U 1 1 5F8A0FBD
P 7100 1650
F 0 "R57" V 6893 1650 50  0000 C CNN
F 1 "R100" V 6984 1650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 1650 50  0001 C CNN
F 3 "~" H 7100 1650 50  0001 C CNN
	1    7100 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 1050 6200 1050
Wire Wire Line
	6700 1150 6200 1150
Wire Wire Line
	6950 1250 6200 1250
Wire Wire Line
	6450 1450 6200 1450
Wire Wire Line
	6700 1550 6200 1550
Wire Wire Line
	6950 1650 6200 1650
$Comp
L led-cube-rescue:R-Device R56
U 1 1 5F8C6D99
P 6350 2350
F 0 "R56" V 6143 2350 50  0000 C CNN
F 1 "R100" V 6234 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 2350 50  0001 C CNN
F 3 "~" H 6350 2350 50  0001 C CNN
	1    6350 2350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R55
U 1 1 5F8C6D9F
P 6600 2450
F 0 "R55" V 6393 2450 50  0000 C CNN
F 1 "R100" V 6485 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 2450 50  0001 C CNN
F 3 "~" H 6600 2450 50  0001 C CNN
	1    6600 2450
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R54
U 1 1 5F8C6DA6
P 6850 2550
F 0 "R54" V 6643 2550 50  0000 C CNN
F 1 "R100" V 6735 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 2550 50  0001 C CNN
F 3 "~" H 6850 2550 50  0001 C CNN
	1    6850 2550
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R53
U 1 1 5F8C6DAC
P 7100 2650
F 0 "R53" V 6893 2650 50  0000 C CNN
F 1 "R100" V 6985 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 2650 50  0001 C CNN
F 3 "~" H 7100 2650 50  0001 C CNN
	1    7100 2650
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R52
U 1 1 5F8C6DB2
P 6350 2750
F 0 "R52" V 6143 2750 50  0000 C CNN
F 1 "R100" V 6235 2750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 2750 50  0001 C CNN
F 3 "~" H 6350 2750 50  0001 C CNN
	1    6350 2750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R51
U 1 1 5F8C6DB8
P 6600 2850
F 0 "R51" V 6393 2850 50  0000 C CNN
F 1 "R100" V 6485 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 2850 50  0001 C CNN
F 3 "~" H 6600 2850 50  0001 C CNN
	1    6600 2850
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R50
U 1 1 5F8C6DBE
P 6850 2950
F 0 "R50" V 6643 2950 50  0000 C CNN
F 1 "R100" V 6735 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 2950 50  0001 C CNN
F 3 "~" H 6850 2950 50  0001 C CNN
	1    6850 2950
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R49
U 1 1 5F8C6DC4
P 7100 3050
F 0 "R49" V 6893 3050 50  0000 C CNN
F 1 "R100" V 6984 3050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 3050 50  0001 C CNN
F 3 "~" H 7100 3050 50  0001 C CNN
	1    7100 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 2450 6200 2450
Wire Wire Line
	6700 2550 6200 2550
Wire Wire Line
	6950 2650 6200 2650
Wire Wire Line
	6450 2850 6200 2850
Wire Wire Line
	6700 2950 6200 2950
Wire Wire Line
	6950 3050 6200 3050
Wire Wire Line
	6500 950  7250 950 
Wire Wire Line
	6750 1050 7250 1050
Wire Wire Line
	7000 1150 7250 1150
Wire Wire Line
	6500 1350 7250 1350
Wire Wire Line
	6750 1450 7250 1450
Wire Wire Line
	7000 1550 7250 1550
Wire Wire Line
	6500 2350 7250 2350
Wire Wire Line
	6750 2450 7250 2450
Wire Wire Line
	7000 2550 7250 2550
Wire Wire Line
	6500 2750 7250 2750
Wire Wire Line
	6750 2850 7250 2850
Wire Wire Line
	7000 2950 7250 2950
Wire Wire Line
	6200 1850 6200 2100
Wire Wire Line
	6200 2100 5350 2100
$Comp
L led-cube-rescue:74AHC595-74xx U7_X7
U 1 1 5F8C6D93
P 5800 2750
F 0 "U7_X7" H 5950 3300 50  0000 C CNN
F 1 "74AHC595" V 5800 2600 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 5800 2750 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 5800 2750 50  0001 C CNN
	1    5800 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 2100 5350 2350
Wire Wire Line
	5400 2350 5350 2350
$Comp
L led-cube-rescue:74AHC595-74xx U6_X6
U 1 1 5FA23CD5
P 5800 4150
F 0 "U6_X6" H 5950 4700 50  0000 C CNN
F 1 "74AHC595" V 5800 4000 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 5800 4150 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 5800 4150 50  0001 C CNN
	1    5800 4150
	1    0    0    -1  
$EndComp
$Comp
L led-cube-rescue:R-Device R48
U 1 1 5FA23CDB
P 6350 3750
F 0 "R48" V 6143 3750 50  0000 C CNN
F 1 "R100" V 6234 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 3750 50  0001 C CNN
F 3 "~" H 6350 3750 50  0001 C CNN
	1    6350 3750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R47
U 1 1 5FA23CE1
P 6600 3850
F 0 "R47" V 6393 3850 50  0000 C CNN
F 1 "R100" V 6485 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 3850 50  0001 C CNN
F 3 "~" H 6600 3850 50  0001 C CNN
	1    6600 3850
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R46
U 1 1 5FA23CE8
P 6850 3950
F 0 "R46" V 6643 3950 50  0000 C CNN
F 1 "R100" V 6735 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 3950 50  0001 C CNN
F 3 "~" H 6850 3950 50  0001 C CNN
	1    6850 3950
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R45
U 1 1 5FA23CEE
P 7100 4050
F 0 "R45" V 6893 4050 50  0000 C CNN
F 1 "R100" V 6985 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 4050 50  0001 C CNN
F 3 "~" H 7100 4050 50  0001 C CNN
	1    7100 4050
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R44
U 1 1 5FA23CF4
P 6350 4150
F 0 "R44" V 6143 4150 50  0000 C CNN
F 1 "R100" V 6235 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 4150 50  0001 C CNN
F 3 "~" H 6350 4150 50  0001 C CNN
	1    6350 4150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R43
U 1 1 5FA23CFA
P 6600 4250
F 0 "R43" V 6393 4250 50  0000 C CNN
F 1 "R100" V 6485 4250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 4250 50  0001 C CNN
F 3 "~" H 6600 4250 50  0001 C CNN
	1    6600 4250
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R42
U 1 1 5FA23D00
P 6850 4350
F 0 "R42" V 6643 4350 50  0000 C CNN
F 1 "R100" V 6735 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 4350 50  0001 C CNN
F 3 "~" H 6850 4350 50  0001 C CNN
	1    6850 4350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R41
U 1 1 5FA23D06
P 7100 4450
F 0 "R41" V 6893 4450 50  0000 C CNN
F 1 "R100" V 6984 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 4450 50  0001 C CNN
F 3 "~" H 7100 4450 50  0001 C CNN
	1    7100 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 3850 6200 3850
Wire Wire Line
	6700 3950 6200 3950
Wire Wire Line
	6950 4050 6200 4050
Wire Wire Line
	6450 4250 6200 4250
Wire Wire Line
	6700 4350 6200 4350
Wire Wire Line
	6950 4450 6200 4450
$Comp
L led-cube-rescue:R-Device R40
U 1 1 5FA23D12
P 6350 5150
F 0 "R40" V 6143 5150 50  0000 C CNN
F 1 "R100" V 6234 5150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 5150 50  0001 C CNN
F 3 "~" H 6350 5150 50  0001 C CNN
	1    6350 5150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R39
U 1 1 5FA23D18
P 6600 5250
F 0 "R39" V 6393 5250 50  0000 C CNN
F 1 "R100" V 6485 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 5250 50  0001 C CNN
F 3 "~" H 6600 5250 50  0001 C CNN
	1    6600 5250
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R38
U 1 1 5FA23D1F
P 6850 5350
F 0 "R38" V 6643 5350 50  0000 C CNN
F 1 "R100" V 6735 5350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 5350 50  0001 C CNN
F 3 "~" H 6850 5350 50  0001 C CNN
	1    6850 5350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R37
U 1 1 5FA23D25
P 7100 5450
F 0 "R37" V 6893 5450 50  0000 C CNN
F 1 "R100" V 6985 5450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 5450 50  0001 C CNN
F 3 "~" H 7100 5450 50  0001 C CNN
	1    7100 5450
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R36
U 1 1 5FA23D2B
P 6350 5550
F 0 "R36" V 6143 5550 50  0000 C CNN
F 1 "R100" V 6235 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6280 5550 50  0001 C CNN
F 3 "~" H 6350 5550 50  0001 C CNN
	1    6350 5550
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R35
U 1 1 5FA23D31
P 6600 5650
F 0 "R35" V 6393 5650 50  0000 C CNN
F 1 "R100" V 6485 5650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6530 5650 50  0001 C CNN
F 3 "~" H 6600 5650 50  0001 C CNN
	1    6600 5650
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R34
U 1 1 5FA23D37
P 6850 5750
F 0 "R34" V 6643 5750 50  0000 C CNN
F 1 "R100" V 6735 5750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6780 5750 50  0001 C CNN
F 3 "~" H 6850 5750 50  0001 C CNN
	1    6850 5750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R33
U 1 1 5FA23D3D
P 7100 5850
F 0 "R33" V 6893 5850 50  0000 C CNN
F 1 "R100" V 6984 5850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7030 5850 50  0001 C CNN
F 3 "~" H 7100 5850 50  0001 C CNN
	1    7100 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 3750 7250 3750
Wire Wire Line
	6750 3850 7250 3850
Wire Wire Line
	7000 3950 7250 3950
Wire Wire Line
	6500 4150 7250 4150
Wire Wire Line
	6750 4250 7250 4250
Wire Wire Line
	7000 4350 7250 4350
Wire Wire Line
	6500 5150 7250 5150
Wire Wire Line
	6750 5250 7250 5250
Wire Wire Line
	7000 5350 7250 5350
Wire Wire Line
	6500 5550 7250 5550
Wire Wire Line
	6750 5650 7250 5650
Wire Wire Line
	7000 5750 7250 5750
Wire Wire Line
	6200 4650 6200 4900
Wire Wire Line
	6200 4900 5350 4900
Wire Wire Line
	5350 4900 5350 5150
Wire Wire Line
	5400 5150 5350 5150
Wire Wire Line
	6200 3250 6200 3500
Wire Wire Line
	6200 3500 5350 3500
Wire Wire Line
	5350 3500 5350 3750
Wire Wire Line
	5350 3750 5400 3750
Wire Wire Line
	5800 2150 5250 2150
Wire Wire Line
	5800 3550 5250 3550
Connection ~ 5250 2150
Wire Wire Line
	5800 4950 5250 4950
Connection ~ 5250 3550
Connection ~ 5250 4950
$Comp
L led-cube-rescue:74AHC595-74xx U4_X4
U 1 1 5FBFE8F7
P 9000 1350
F 0 "U4_X4" H 9150 1900 50  0000 C CNN
F 1 "74AHC595" V 9000 1200 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 9000 1350 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 9000 1350 50  0001 C CNN
	1    9000 1350
	1    0    0    -1  
$EndComp
$Comp
L led-cube-rescue:R-Device R32
U 1 1 5FBFE8FD
P 9550 950
F 0 "R32" V 9343 950 50  0000 C CNN
F 1 "R100" V 9434 950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 950 50  0001 C CNN
F 3 "~" H 9550 950 50  0001 C CNN
	1    9550 950 
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R31
U 1 1 5FBFE903
P 9800 1050
F 0 "R31" V 9593 1050 50  0000 C CNN
F 1 "R100" V 9685 1050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 1050 50  0001 C CNN
F 3 "~" H 9800 1050 50  0001 C CNN
	1    9800 1050
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R30
U 1 1 5FBFE90A
P 10050 1150
F 0 "R30" V 9843 1150 50  0000 C CNN
F 1 "R100" V 9935 1150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 1150 50  0001 C CNN
F 3 "~" H 10050 1150 50  0001 C CNN
	1    10050 1150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R29
U 1 1 5FBFE910
P 10300 1250
F 0 "R29" V 10093 1250 50  0000 C CNN
F 1 "R100" V 10185 1250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 1250 50  0001 C CNN
F 3 "~" H 10300 1250 50  0001 C CNN
	1    10300 1250
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R28
U 1 1 5FBFE916
P 9550 1350
F 0 "R28" V 9343 1350 50  0000 C CNN
F 1 "R100" V 9435 1350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 1350 50  0001 C CNN
F 3 "~" H 9550 1350 50  0001 C CNN
	1    9550 1350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R27
U 1 1 5FBFE91C
P 9800 1450
F 0 "R27" V 9593 1450 50  0000 C CNN
F 1 "R100" V 9685 1450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 1450 50  0001 C CNN
F 3 "~" H 9800 1450 50  0001 C CNN
	1    9800 1450
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R26
U 1 1 5FBFE922
P 10050 1550
F 0 "R26" V 9843 1550 50  0000 C CNN
F 1 "R100" V 9935 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 1550 50  0001 C CNN
F 3 "~" H 10050 1550 50  0001 C CNN
	1    10050 1550
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R25
U 1 1 5FBFE928
P 10300 1650
F 0 "R25" V 10093 1650 50  0000 C CNN
F 1 "R100" V 10184 1650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 1650 50  0001 C CNN
F 3 "~" H 10300 1650 50  0001 C CNN
	1    10300 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 1050 9400 1050
Wire Wire Line
	9900 1150 9400 1150
Wire Wire Line
	10150 1250 9400 1250
Wire Wire Line
	9650 1450 9400 1450
Wire Wire Line
	9900 1550 9400 1550
Wire Wire Line
	10150 1650 9400 1650
$Comp
L led-cube-rescue:R-Device R24
U 1 1 5FBFE934
P 9550 2350
F 0 "R24" V 9343 2350 50  0000 C CNN
F 1 "R100" V 9434 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 2350 50  0001 C CNN
F 3 "~" H 9550 2350 50  0001 C CNN
	1    9550 2350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R23
U 1 1 5FBFE93A
P 9800 2450
F 0 "R23" V 9593 2450 50  0000 C CNN
F 1 "R100" V 9685 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 2450 50  0001 C CNN
F 3 "~" H 9800 2450 50  0001 C CNN
	1    9800 2450
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R22
U 1 1 5FBFE941
P 10050 2550
F 0 "R22" V 9843 2550 50  0000 C CNN
F 1 "R100" V 9935 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 2550 50  0001 C CNN
F 3 "~" H 10050 2550 50  0001 C CNN
	1    10050 2550
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R21
U 1 1 5FBFE947
P 10300 2650
F 0 "R21" V 10093 2650 50  0000 C CNN
F 1 "R100" V 10185 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 2650 50  0001 C CNN
F 3 "~" H 10300 2650 50  0001 C CNN
	1    10300 2650
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R20
U 1 1 5FBFE94D
P 9550 2750
F 0 "R20" V 9343 2750 50  0000 C CNN
F 1 "R100" V 9435 2750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 2750 50  0001 C CNN
F 3 "~" H 9550 2750 50  0001 C CNN
	1    9550 2750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R19
U 1 1 5FBFE953
P 9800 2850
F 0 "R19" V 9593 2850 50  0000 C CNN
F 1 "R100" V 9685 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 2850 50  0001 C CNN
F 3 "~" H 9800 2850 50  0001 C CNN
	1    9800 2850
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R18
U 1 1 5FBFE959
P 10050 2950
F 0 "R18" V 9843 2950 50  0000 C CNN
F 1 "R100" V 9935 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 2950 50  0001 C CNN
F 3 "~" H 10050 2950 50  0001 C CNN
	1    10050 2950
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R17
U 1 1 5FBFE95F
P 10300 3050
F 0 "R17" V 10093 3050 50  0000 C CNN
F 1 "R100" V 10184 3050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 3050 50  0001 C CNN
F 3 "~" H 10300 3050 50  0001 C CNN
	1    10300 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 2450 9400 2450
Wire Wire Line
	9900 2550 9400 2550
Wire Wire Line
	10150 2650 9400 2650
Wire Wire Line
	9650 2850 9400 2850
Wire Wire Line
	9900 2950 9400 2950
Wire Wire Line
	10150 3050 9400 3050
Wire Wire Line
	9700 950  10450 950 
Wire Wire Line
	10200 1150 10450 1150
Wire Wire Line
	9700 1350 10450 1350
Wire Wire Line
	9950 1450 10450 1450
Wire Wire Line
	10200 1550 10450 1550
Wire Wire Line
	9700 2350 10450 2350
Wire Wire Line
	9950 2450 10450 2450
Wire Wire Line
	10200 2550 10450 2550
Wire Wire Line
	9700 2750 10450 2750
Wire Wire Line
	9950 2850 10450 2850
Wire Wire Line
	10200 2950 10450 2950
Wire Wire Line
	9400 1850 9400 2100
Wire Wire Line
	9400 2100 8550 2100
$Comp
L led-cube-rescue:74AHC595-74xx U3_X3
U 1 1 5FBFE979
P 9000 2750
F 0 "U3_X3" H 9150 3300 50  0000 C CNN
F 1 "74AHC595" V 9000 2600 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 9000 2750 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 9000 2750 50  0001 C CNN
	1    9000 2750
	1    0    0    -1  
$EndComp
$Comp
L led-cube-rescue:74AHC595-74xx U2_X2
U 1 1 5FBFE97F
P 9000 4150
F 0 "U2_X2" H 9150 4700 50  0000 C CNN
F 1 "74AHC595" V 9000 4000 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 9000 4150 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 9000 4150 50  0001 C CNN
	1    9000 4150
	1    0    0    -1  
$EndComp
$Comp
L led-cube-rescue:R-Device R16
U 1 1 5FBFE985
P 9550 3750
F 0 "R16" V 9343 3750 50  0000 C CNN
F 1 "R100" V 9434 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 3750 50  0001 C CNN
F 3 "~" H 9550 3750 50  0001 C CNN
	1    9550 3750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R15
U 1 1 5FBFE98B
P 9800 3850
F 0 "R15" V 9593 3850 50  0000 C CNN
F 1 "R100" V 9685 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 3850 50  0001 C CNN
F 3 "~" H 9800 3850 50  0001 C CNN
	1    9800 3850
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R14
U 1 1 5FBFE992
P 10050 3950
F 0 "R14" V 9843 3950 50  0000 C CNN
F 1 "R100" V 9935 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 3950 50  0001 C CNN
F 3 "~" H 10050 3950 50  0001 C CNN
	1    10050 3950
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R13
U 1 1 5FBFE998
P 10300 4050
F 0 "R13" V 10093 4050 50  0000 C CNN
F 1 "R100" V 10185 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 4050 50  0001 C CNN
F 3 "~" H 10300 4050 50  0001 C CNN
	1    10300 4050
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R12
U 1 1 5FBFE99E
P 9550 4150
F 0 "R12" V 9343 4150 50  0000 C CNN
F 1 "R100" V 9435 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 4150 50  0001 C CNN
F 3 "~" H 9550 4150 50  0001 C CNN
	1    9550 4150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R11
U 1 1 5FBFE9A4
P 9800 4250
F 0 "R11" V 9593 4250 50  0000 C CNN
F 1 "R100" V 9685 4250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 4250 50  0001 C CNN
F 3 "~" H 9800 4250 50  0001 C CNN
	1    9800 4250
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R10
U 1 1 5FBFE9AA
P 10050 4350
F 0 "R10" V 9843 4350 50  0000 C CNN
F 1 "R100" V 9935 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 4350 50  0001 C CNN
F 3 "~" H 10050 4350 50  0001 C CNN
	1    10050 4350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R9
U 1 1 5FBFE9B0
P 10300 4450
F 0 "R9" V 10093 4450 50  0000 C CNN
F 1 "R100" V 10184 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 4450 50  0001 C CNN
F 3 "~" H 10300 4450 50  0001 C CNN
	1    10300 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 3850 9400 3850
Wire Wire Line
	9900 3950 9400 3950
Wire Wire Line
	10150 4050 9400 4050
Wire Wire Line
	9650 4250 9400 4250
Wire Wire Line
	9900 4350 9400 4350
Wire Wire Line
	10150 4450 9400 4450
$Comp
L led-cube-rescue:R-Device R8
U 1 1 5FBFE9BC
P 9550 5150
F 0 "R8" V 9343 5150 50  0000 C CNN
F 1 "R100" V 9434 5150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 5150 50  0001 C CNN
F 3 "~" H 9550 5150 50  0001 C CNN
	1    9550 5150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R7
U 1 1 5FBFE9C2
P 9800 5250
F 0 "R7" V 9593 5250 50  0000 C CNN
F 1 "R100" V 9685 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 5250 50  0001 C CNN
F 3 "~" H 9800 5250 50  0001 C CNN
	1    9800 5250
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R6
U 1 1 5FBFE9C9
P 10050 5350
F 0 "R6" V 9843 5350 50  0000 C CNN
F 1 "R100" V 9935 5350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 5350 50  0001 C CNN
F 3 "~" H 10050 5350 50  0001 C CNN
	1    10050 5350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R5
U 1 1 5FBFE9CF
P 10300 5450
F 0 "R5" V 10093 5450 50  0000 C CNN
F 1 "R100" V 10185 5450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 5450 50  0001 C CNN
F 3 "~" H 10300 5450 50  0001 C CNN
	1    10300 5450
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R4
U 1 1 5FBFE9D5
P 9550 5550
F 0 "R4" V 9343 5550 50  0000 C CNN
F 1 "R100" V 9435 5550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9480 5550 50  0001 C CNN
F 3 "~" H 9550 5550 50  0001 C CNN
	1    9550 5550
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R3
U 1 1 5FBFE9DB
P 9800 5650
F 0 "R3" V 9593 5650 50  0000 C CNN
F 1 "R100" V 9685 5650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9730 5650 50  0001 C CNN
F 3 "~" H 9800 5650 50  0001 C CNN
	1    9800 5650
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R2
U 1 1 5FBFE9E1
P 10050 5750
F 0 "R2" V 9843 5750 50  0000 C CNN
F 1 "R100" V 9935 5750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 5750 50  0001 C CNN
F 3 "~" H 10050 5750 50  0001 C CNN
	1    10050 5750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R1
U 1 1 5FBFE9E7
P 10300 5850
F 0 "R1" V 10093 5850 50  0000 C CNN
F 1 "R100" V 10184 5850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10230 5850 50  0001 C CNN
F 3 "~" H 10300 5850 50  0001 C CNN
	1    10300 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	9650 5250 9400 5250
Wire Wire Line
	9900 5350 9400 5350
Wire Wire Line
	10150 5450 9400 5450
Wire Wire Line
	9650 5650 9400 5650
Wire Wire Line
	9900 5750 9400 5750
Wire Wire Line
	10150 5850 9400 5850
Wire Wire Line
	9700 3750 10450 3750
Wire Wire Line
	9950 3850 10450 3850
Wire Wire Line
	10200 3950 10450 3950
Wire Wire Line
	9700 4150 10450 4150
Wire Wire Line
	9950 4250 10450 4250
Wire Wire Line
	10200 4350 10450 4350
Wire Wire Line
	9700 5150 10450 5150
Wire Wire Line
	9950 5250 10450 5250
Wire Wire Line
	10200 5350 10450 5350
Wire Wire Line
	9700 5550 10450 5550
Wire Wire Line
	9950 5650 10450 5650
Wire Wire Line
	10200 5750 10450 5750
Wire Wire Line
	9400 4650 9400 4900
$Comp
L led-cube-rescue:74AHC595-74xx U1_X1
U 1 1 5FBFEA01
P 9000 5550
F 0 "U1_X1" H 9150 6100 50  0000 C CNN
F 1 "74AHC595" V 9000 5400 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 9000 5550 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 9000 5550 50  0001 C CNN
	1    9000 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 3250 9400 3500
Wire Wire Line
	9400 3500 8550 3500
Wire Wire Line
	9000 4850 8000 4850
Wire Wire Line
	9000 3450 8000 3450
Wire Wire Line
	9000 2050 8000 2050
Wire Wire Line
	9000 2150 8450 2150
Wire Wire Line
	9000 3550 8450 3550
Wire Wire Line
	9000 4950 8450 4950
Connection ~ 8000 3450
Wire Wire Line
	8000 4850 8000 5750
Connection ~ 8000 4850
Wire Wire Line
	9400 4900 8550 4900
Wire Wire Line
	8600 5150 8550 5150
Wire Wire Line
	8550 5150 8550 4900
Wire Wire Line
	8600 3750 8550 3750
Wire Wire Line
	8550 3750 8550 3500
Wire Wire Line
	8550 2100 8550 2350
Wire Wire Line
	8550 2350 8600 2350
Wire Wire Line
	8450 2150 8450 1250
Wire Wire Line
	8450 2150 8450 2650
Connection ~ 8450 2150
Wire Wire Line
	8450 3550 8450 4050
Connection ~ 8450 3550
Wire Wire Line
	7850 950  7850 6050
Wire Wire Line
	8600 1550 8000 1550
Wire Wire Line
	8000 1550 8000 2050
Connection ~ 8000 2050
Wire Wire Line
	8600 2950 8000 2950
Wire Wire Line
	8000 2050 8000 2950
Connection ~ 8000 2950
Wire Wire Line
	8000 2950 8000 3450
Wire Wire Line
	8600 4350 8000 4350
Wire Wire Line
	8000 3450 8000 4350
Connection ~ 8000 4350
Wire Wire Line
	8000 4350 8000 4850
Wire Wire Line
	8600 5750 8000 5750
Connection ~ 8000 5750
Wire Wire Line
	5250 750  5250 1250
Wire Wire Line
	5400 1250 5250 1250
Connection ~ 5250 1250
Wire Wire Line
	5250 1250 5250 2150
Wire Wire Line
	5400 2650 5250 2650
Connection ~ 5250 2650
Wire Wire Line
	5250 2650 5250 3550
Wire Wire Line
	5400 4050 5250 4050
Connection ~ 5250 4050
Wire Wire Line
	5250 4050 5250 4950
Wire Wire Line
	5400 5450 5250 5450
Connection ~ 5250 5450
Wire Wire Line
	8600 5450 8450 5450
Wire Wire Line
	8450 5450 8450 4950
Connection ~ 8450 4950
Wire Wire Line
	8600 4050 8450 4050
Connection ~ 8450 4050
Wire Wire Line
	8450 4050 8450 4950
Wire Wire Line
	8600 2650 8450 2650
Connection ~ 8450 2650
Wire Wire Line
	8450 2650 8450 3550
Wire Wire Line
	8600 1250 8450 1250
Connection ~ 8450 1250
Wire Wire Line
	8450 1250 8450 750 
Wire Wire Line
	9950 1050 10450 1050
Wire Wire Line
	8600 5350 8350 5350
Wire Wire Line
	8350 5350 8350 3950
Wire Wire Line
	8350 3950 8600 3950
Wire Wire Line
	8600 2550 8350 2550
Wire Wire Line
	8350 2550 8350 3950
Connection ~ 8350 3950
Wire Wire Line
	8600 1150 8350 1150
Wire Wire Line
	8350 1150 8350 2550
Connection ~ 8350 2550
Wire Wire Line
	5150 5350 5150 3950
Connection ~ 5150 3950
Connection ~ 5150 5350
Wire Wire Line
	5800 750  5250 750 
Connection ~ 5800 750 
Connection ~ 5250 750 
Wire Wire Line
	4800 6850 4850 6850
Wire Wire Line
	4400 7650 5250 7650
Connection ~ 5250 7150
Wire Wire Line
	5250 7150 5250 7650
Wire Wire Line
	8600 4250 8250 4250
Wire Wire Line
	8250 4250 8250 5650
Wire Wire Line
	8250 5650 8600 5650
Wire Wire Line
	8600 2850 8250 2850
Wire Wire Line
	8250 2850 8250 4250
Connection ~ 8250 4250
Wire Wire Line
	8600 1450 8250 1450
Wire Wire Line
	8250 1450 8250 2850
Connection ~ 8250 2850
Connection ~ 8250 5650
Wire Wire Line
	5400 1150 5150 1150
Wire Wire Line
	5050 1450 5400 1450
Wire Wire Line
	5050 2850 5400 2850
Connection ~ 5050 2850
Wire Wire Line
	5050 4250 5400 4250
Connection ~ 5050 4250
Wire Wire Line
	5050 5650 5400 5650
Connection ~ 5050 5650
Wire Wire Line
	5250 2150 5250 2650
Wire Wire Line
	5150 1150 5150 2550
Wire Wire Line
	5050 1450 5050 2850
Wire Wire Line
	5400 2550 5150 2550
Connection ~ 5150 2550
Wire Wire Line
	5250 3550 5250 4050
Wire Wire Line
	5150 3950 5400 3950
Wire Wire Line
	4400 4350 5400 4350
Connection ~ 4400 4350
Wire Wire Line
	4400 4350 4400 4850
Wire Wire Line
	4400 3450 5800 3450
Connection ~ 4400 3450
Wire Wire Line
	4400 3450 4400 3600
Wire Wire Line
	4400 2950 5400 2950
Connection ~ 4400 2950
Wire Wire Line
	4400 2050 5800 2050
Connection ~ 4400 2050
Wire Wire Line
	5250 4950 5250 5450
Wire Wire Line
	5150 5350 5400 5350
Wire Wire Line
	5050 4250 5050 5650
Wire Wire Line
	4800 6950 5050 6950
Wire Wire Line
	4800 7150 5250 7150
Wire Wire Line
	4800 7250 5150 7250
Wire Wire Line
	4400 5750 5400 5750
Connection ~ 4400 5750
Wire Wire Line
	4400 4850 5800 4850
Connection ~ 4400 4850
Wire Wire Line
	4400 4850 4400 5750
Wire Wire Line
	5150 5350 5150 7250
Wire Wire Line
	8350 1150 8350 650 
Wire Wire Line
	5150 650  5150 1150
Connection ~ 8350 1150
Connection ~ 5150 1150
Text Label 3300 3350 0    50   ~ 0
D13-SCK<-->SRCLK
Text Label 3300 3200 0    50   ~ 0
D10-SS<-->RCLK
Text Label 3300 3050 0    50   ~ 0
D11-MOSI<-->SER
Wire Wire Line
	850  850  850  750 
Wire Wire Line
	850  750  1050 750 
Wire Wire Line
	5250 5450 5250 6550
Wire Wire Line
	5700 6550 5800 6550
Wire Wire Line
	5400 6550 5250 6550
Connection ~ 5250 6550
Wire Wire Line
	5250 6550 5250 7150
Wire Wire Line
	5400 1550 4400 1550
Wire Wire Line
	3700 6400 3400 6400
Wire Wire Line
	3800 5100 3400 5100
Wire Wire Line
	3850 4450 3400 4450
Wire Wire Line
	3850 4450 3850 6750
Wire Wire Line
	3800 5100 3800 6850
Wire Wire Line
	4000 6750 3850 6750
Wire Wire Line
	4000 6850 3800 6850
Wire Wire Line
	4000 6950 3750 6950
Wire Wire Line
	4000 7050 3700 7050
Connection ~ 8450 750 
Wire Wire Line
	8450 750  9000 750 
Wire Wire Line
	5150 650  8350 650 
Wire Wire Line
	4400 2050 4400 2950
Connection ~ 1050 750 
Wire Wire Line
	4400 2950 4400 3450
Wire Wire Line
	1050 750  1300 750 
Wire Wire Line
	4400 1550 4400 2050
Wire Wire Line
	2500 1950 2150 1950
Connection ~ 3000 3600
Wire Wire Line
	1900 750  2900 750 
$Comp
L led-cube-rescue:Arduino_Nano_v3.x-MCU_Module A1
U 1 1 606041E0
P 3000 1850
F 0 "A1" H 3000 761 50  0001 C CNN
F 1 "Arduino_Nano_v3.x" V 3000 1850 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 3000 1850 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 3000 1850 50  0001 C CNN
	1    3000 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 850  2900 750 
Connection ~ 4400 3600
Wire Wire Line
	4400 3600 4400 4350
Wire Wire Line
	2350 2350 2500 2350
Wire Wire Line
	4200 950  4200 3050
Wire Wire Line
	2200 2250 2500 2250
Wire Wire Line
	4200 950  5400 950 
Connection ~ 5150 3350
Wire Wire Line
	5150 3350 5150 3950
Wire Wire Line
	5150 2550 5150 3350
Connection ~ 5050 3200
Wire Wire Line
	5050 3200 5050 4250
Wire Wire Line
	5050 2850 5050 3200
Wire Wire Line
	2200 3200 5050 3200
Wire Wire Line
	2350 2350 2350 3050
Wire Wire Line
	2350 3050 4200 3050
Wire Wire Line
	2200 2250 2200 3150
Wire Wire Line
	2500 2550 2500 3250
Wire Wire Line
	2500 3350 5150 3350
$Comp
L led-cube-rescue:R-Device R_green1
U 1 1 5F9B4101
P 2000 1950
F 0 "R_green1" V 1900 2000 50  0000 C CNN
F 1 "R100" V 1884 1950 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1930 1950 50  0001 C CNN
F 3 "~" H 2000 1950 50  0001 C CNN
	1    2000 1950
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:R-Device R_red1
U 1 1 5F8E0B18
P 2000 1750
F 0 "R_red1" V 1900 1750 50  0000 C CNN
F 1 "R100" V 1884 1750 50  0001 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1930 1750 50  0001 C CNN
F 3 "~" H 2000 1750 50  0001 C CNN
	1    2000 1750
	0    1    1    0   
$EndComp
Connection ~ 2900 750 
Wire Wire Line
	2900 750  5250 750 
Wire Wire Line
	3000 3600 3100 3600
Wire Wire Line
	3600 4150 3600 3600
Connection ~ 1950 7700
Wire Wire Line
	2200 7700 1950 7700
Wire Wire Line
	2200 7450 2200 7700
Connection ~ 1950 7050
Wire Wire Line
	2200 7050 1950 7050
Wire Wire Line
	2200 7350 2200 7050
Connection ~ 1950 6400
Wire Wire Line
	2250 6400 1950 6400
Wire Wire Line
	2250 7250 2250 6400
Connection ~ 1950 5750
Wire Wire Line
	2300 5750 1950 5750
Wire Wire Line
	2300 7150 2300 5750
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z5
U 1 1 5F90BF7C
P 1350 5550
F 0 "Q1_Z5" V 1678 5550 50  0000 C CNN
F 1 "PN2222A" V 1587 5550 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 1550 5475 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1350 5550 50  0001 L CNN
	1    1350 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 5750 1950 5750
Wire Wire Line
	1750 5450 1750 5150
Wire Wire Line
	1750 5150 1150 5150
Wire Wire Line
	1150 5150 1150 5450
Connection ~ 1150 5150
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z6
U 1 1 5F90BF6C
P 1350 6200
F 0 "Q1_Z6" V 1678 6200 50  0000 C CNN
F 1 "PN2222A" V 1587 6200 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 1550 6125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1350 6200 50  0001 L CNN
	1    1350 6200
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z6
U 1 1 5F90BF66
P 1950 6200
F 0 "Q2_Z6" V 2278 6200 50  0000 C CNN
F 1 "PN2222A" V 2187 6200 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 2150 6125 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1950 6200 50  0001 L CNN
	1    1950 6200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 6400 1950 6400
Wire Wire Line
	1750 6100 1750 5800
Wire Wire Line
	1750 5800 1150 5800
Wire Wire Line
	1150 5800 1150 6100
Wire Wire Line
	1150 5800 1050 5800
Connection ~ 1150 5800
Connection ~ 1550 6100
Connection ~ 2150 5450
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z5
U 1 1 5F90BF56
P 1950 5550
F 0 "Q2_Z5" V 2278 5550 50  0000 C CNN
F 1 "PN2222A" V 2187 5550 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 2150 5475 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1950 5550 50  0001 L CNN
	1    1950 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 5450 1550 6100
Wire Wire Line
	2150 5450 2150 6100
Connection ~ 2150 6100
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z7
U 1 1 5F90BF4D
P 1350 6850
F 0 "Q1_Z7" V 1678 6850 50  0000 C CNN
F 1 "PN2222A" V 1587 6850 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 1550 6775 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1350 6850 50  0001 L CNN
	1    1350 6850
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z7
U 1 1 5F90BF47
P 1950 6850
F 0 "Q2_Z7" V 2278 6850 50  0000 C CNN
F 1 "PN2222A" V 2187 6850 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 2150 6775 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1950 6850 50  0001 L CNN
	1    1950 6850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 7050 1950 7050
Wire Wire Line
	1750 6750 1750 6450
Wire Wire Line
	1750 6450 1150 6450
Wire Wire Line
	1150 6450 1150 6750
Connection ~ 1150 6450
Wire Wire Line
	1550 6100 1550 6750
Wire Wire Line
	2150 6100 2150 6750
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q1_Z8
U 1 1 5F90BF38
P 1350 7500
F 0 "Q1_Z8" V 1678 7500 50  0000 C CNN
F 1 "PN2222A" V 1587 7500 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 1550 7425 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1350 7500 50  0001 L CNN
	1    1350 7500
	0    -1   -1   0   
$EndComp
$Comp
L led-cube-rescue:PN2222A-Transistor_BJT Q2_Z8
U 1 1 5F90BF32
P 1950 7500
F 0 "Q2_Z8" V 2278 7500 50  0000 C CNN
F 1 "PN2222A" V 2187 7500 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline_Wide" H 2150 7425 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/PN2222-D.PDF" H 1950 7500 50  0001 L CNN
	1    1950 7500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 7700 1950 7700
Wire Wire Line
	1750 7400 1750 7100
Wire Wire Line
	1750 7100 1150 7100
Wire Wire Line
	1150 7100 1150 7400
Connection ~ 1150 7100
Wire Wire Line
	1550 6750 1550 7400
Wire Wire Line
	2150 6750 2150 7400
Connection ~ 1550 6750
Connection ~ 2150 6750
Wire Wire Line
	4000 7150 2300 7150
Wire Wire Line
	4000 7250 2250 7250
Wire Wire Line
	4000 7350 2200 7350
Wire Wire Line
	4000 7450 2200 7450
Wire Wire Line
	3000 3600 3000 4150
Connection ~ 3600 3600
Wire Wire Line
	3600 3600 4400 3600
Wire Wire Line
	800  5150 1150 5150
Wire Wire Line
	1050 5800 1050 5250
Wire Wire Line
	1050 5250 800  5250
Wire Wire Line
	800  5350 950  5350
Wire Wire Line
	950  5350 950  6450
Wire Wire Line
	950  6450 1150 6450
Wire Wire Line
	800  4750 2400 4750
Wire Wire Line
	2400 3850 2600 3850
Wire Wire Line
	800  4850 2500 4850
Wire Wire Line
	2500 4500 2600 4500
Wire Wire Line
	800  4950 2500 4950
Wire Wire Line
	2150 3600 2150 5450
Connection ~ 2150 3600
Wire Wire Line
	2150 3600 3000 3600
Wire Wire Line
	800  5050 2400 5050
Wire Wire Line
	2400 5050 2400 5800
Wire Wire Line
	2400 5800 2600 5800
Wire Wire Line
	2500 4950 2500 5150
Wire Wire Line
	2500 4850 2500 4500
Wire Wire Line
	2400 4750 2400 3850
Wire Wire Line
	800  5450 850  5450
Wire Wire Line
	850  5450 850  7100
Wire Wire Line
	850  7100 1150 7100
Wire Wire Line
	2000 3050 2350 3050
Connection ~ 2350 3050
Wire Wire Line
	2000 3150 2200 3150
Connection ~ 2200 3150
Wire Wire Line
	2200 3150 2200 3200
Wire Wire Line
	2000 3250 2500 3250
Connection ~ 2500 3250
Wire Wire Line
	2500 3250 2500 3350
Wire Wire Line
	2150 1750 2500 1750
Wire Wire Line
	1050 750  1050 950 
Wire Wire Line
	1150 1850 1050 1850
Connection ~ 1050 1850
Wire Wire Line
	1050 1850 1050 1950
Wire Wire Line
	1150 1950 1050 1950
Connection ~ 1050 1950
Wire Wire Line
	1050 1950 1050 2050
Wire Wire Line
	1150 2050 1050 2050
Connection ~ 1050 2050
Wire Wire Line
	1050 2050 1050 3600
Wire Wire Line
	1850 1750 1750 1750
Wire Wire Line
	1750 1750 1750 1850
$Comp
L led-cube-rescue:Jumper_2_Open-Jumper JP6
U 1 1 606AE735
P 4750 3750
F 0 "JP6" H 4750 3985 50  0000 C CNN
F 1 "Jumper_2_Open" H 4750 3894 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4750 3750 50  0001 C CNN
F 3 "~" H 4750 3750 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3750 4950 3750
Connection ~ 5350 3750
Wire Wire Line
	4200 3050 4200 3750
Wire Wire Line
	4200 3750 4550 3750
Connection ~ 4200 3050
Wire Wire Line
	5350 5150 4950 5150
Connection ~ 5350 5150
Wire Wire Line
	4550 5150 4200 5150
Wire Wire Line
	4200 5150 4200 3750
Connection ~ 4200 3750
Wire Wire Line
	8600 950  7850 950 
$Comp
L led-cube-rescue:Jumper_2_Open-Jumper JP4
U 1 1 607ABF77
P 4750 6050
F 0 "JP4" H 4750 6285 50  0000 C CNN
F 1 "Jumper_2_Open" H 4750 6194 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4750 6050 50  0001 C CNN
F 3 "~" H 4750 6050 50  0001 C CNN
	1    4750 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 5850 6200 5850
Wire Wire Line
	6700 5750 6200 5750
Wire Wire Line
	6450 5650 6200 5650
Wire Wire Line
	6950 5450 6200 5450
Wire Wire Line
	6700 5350 6200 5350
Wire Wire Line
	6450 5250 6200 5250
$Comp
L led-cube-rescue:74AHC595-74xx U5_X5
U 1 1 5FA23D58
P 5800 5550
F 0 "U5_X5" H 6000 6100 50  0000 C CNN
F 1 "74AHC595" V 5800 5400 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm_Socket" H 5800 5550 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74AHC_AHCT595.pdf" H 5800 5550 50  0001 C CNN
	1    5800 5550
	1    0    0    -1  
$EndComp
$Comp
L led-cube-rescue:Jumper_2_Open-Jumper JP5
U 1 1 60714866
P 4750 5150
F 0 "JP5" H 4750 5385 50  0000 C CNN
F 1 "Jumper_2_Open" H 4750 5294 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4750 5150 50  0001 C CNN
F 3 "~" H 4750 5150 50  0001 C CNN
	1    4750 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 6050 5350 6050
Wire Wire Line
	6200 6050 6250 6050
Wire Wire Line
	6250 6050 7850 6050
Connection ~ 6250 6050
Wire Wire Line
	6100 6500 9400 6500
Wire Wire Line
	6100 7450 6100 6500
Wire Wire Line
	9400 6500 9400 6050
Wire Wire Line
	5050 6400 8250 6400
Wire Wire Line
	5050 6400 5050 6950
Connection ~ 5050 6400
Wire Wire Line
	5050 5650 5050 6400
Wire Wire Line
	8250 5650 8250 6400
Wire Wire Line
	5800 6550 5800 6300
Wire Wire Line
	5800 6300 5800 6250
Connection ~ 5800 6300
Wire Wire Line
	8000 5750 8000 6300
Wire Wire Line
	5800 6300 8000 6300
Wire Wire Line
	8000 6300 9000 6300
Connection ~ 8000 6300
Wire Wire Line
	9000 6300 9000 6250
Wire Wire Line
	5800 6300 4850 6300
Wire Wire Line
	4850 6850 4850 6300
Connection ~ 4850 6300
Wire Wire Line
	4400 6300 4850 6300
Wire Wire Line
	4400 6300 4400 6350
Connection ~ 4400 6300
Wire Wire Line
	5350 6050 5350 6250
Wire Wire Line
	6250 6050 6250 6250
Wire Wire Line
	5350 6250 6250 6250
Wire Wire Line
	4400 5750 4400 6300
Wire Wire Line
	4200 5150 4200 6050
Wire Wire Line
	4200 6050 4550 6050
Connection ~ 4200 5150
Text Label 1900 2150 0    50   ~ 0
BUTTON1
$Comp
L led-cube-rescue:Screw_Terminal_01x02-Connector J_power1
U 1 1 60C14638
P 1500 1150
F 0 "J_power1" V 1600 1000 50  0000 L CNN
F 1 "Screw_Terminal_01x02" V 1700 1000 50  0001 L CNN
F 2 "TerminalBlock:TerminalBlock_Altech_AK300-2_P5.00mm" H 1500 1150 50  0001 C CNN
F 3 "~" H 1500 1150 50  0001 C CNN
	1    1500 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 950  1900 950 
Wire Wire Line
	1900 950  1900 750 
Wire Wire Line
	1400 950  1050 950 
Connection ~ 1050 950 
Wire Wire Line
	1050 950  1050 1850
Wire Wire Line
	2500 2050 1650 2050
Wire Wire Line
	1850 1950 1650 1950
Wire Wire Line
	1750 1850 1650 1850
$Comp
L led-cube-rescue:MountingHole_Pad-Mechanical H1
U 1 1 5FAD646B
P 1150 3750
F 0 "H1" V 1104 3900 50  0000 L CNN
F 1 "MountingHole_Pad" V 1195 3900 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1150 3750 50  0001 C CNN
F 3 "~" H 1150 3750 50  0001 C CNN
	1    1150 3750
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:MountingHole_Pad-Mechanical H3
U 1 1 5FAD773E
P 1150 4150
F 0 "H3" V 1104 4300 50  0000 L CNN
F 1 "MountingHole_Pad" V 1195 4300 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1150 4150 50  0001 C CNN
F 3 "~" H 1150 4150 50  0001 C CNN
	1    1150 4150
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:MountingHole_Pad-Mechanical H4
U 1 1 5FAD791C
P 1150 4350
F 0 "H4" V 1104 4500 50  0000 L CNN
F 1 "MountingHole_Pad" V 1195 4500 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1150 4350 50  0001 C CNN
F 3 "~" H 1150 4350 50  0001 C CNN
	1    1150 4350
	0    1    1    0   
$EndComp
$Comp
L led-cube-rescue:MountingHole_Pad-Mechanical H2
U 1 1 5FAD748F
P 1150 3950
F 0 "H2" V 1104 4100 50  0000 L CNN
F 1 "MountingHole_Pad" V 1195 4100 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1150 3950 50  0001 C CNN
F 3 "~" H 1150 3950 50  0001 C CNN
	1    1150 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 3750 1050 3950
Connection ~ 1050 3950
Wire Wire Line
	1050 3950 1050 4150
Connection ~ 1050 4150
Wire Wire Line
	1050 4150 1050 4350
Wire Wire Line
	1050 3750 1050 3600
Connection ~ 1050 3750
Connection ~ 1050 3600
Wire Wire Line
	1050 3600 2150 3600
Wire Wire Line
	1550 5450 1500 5450
Wire Wire Line
	1500 5450 1500 4650
Wire Wire Line
	1500 4650 1050 4650
Wire Wire Line
	1050 4650 1050 4350
Connection ~ 1550 5450
Connection ~ 1050 4350
NoConn ~ 3500 1350
NoConn ~ 3500 1650
NoConn ~ 3500 1250
NoConn ~ 3500 1850
NoConn ~ 3500 1950
NoConn ~ 3500 2050
NoConn ~ 3500 2150
NoConn ~ 3500 2250
NoConn ~ 3500 2350
NoConn ~ 3500 2450
NoConn ~ 3500 2550
NoConn ~ 2500 2150
NoConn ~ 2500 1850
NoConn ~ 2500 1650
NoConn ~ 2500 1550
NoConn ~ 2500 1450
NoConn ~ 2500 1350
NoConn ~ 2500 1250
NoConn ~ 3200 850 
NoConn ~ 3100 850 
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J8
U 1 1 60268218
P 7450 1350
F 0 "J8" H 7368 725 50  0000 C CNN
F 1 "Conn_01x08" H 7368 816 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7450 1350 50  0001 C CNN
F 3 "~" H 7450 1350 50  0001 C CNN
	1    7450 1350
	1    0    0    1   
$EndComp
Wire Wire Line
	5800 750  8450 750 
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J7
U 1 1 6030E1AA
P 7450 2750
F 0 "J7" H 7368 2125 50  0000 C CNN
F 1 "Conn_01x08" H 7368 2216 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7450 2750 50  0001 C CNN
F 3 "~" H 7450 2750 50  0001 C CNN
	1    7450 2750
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J6
U 1 1 6030F3CF
P 7450 4150
F 0 "J6" H 7368 3525 50  0000 C CNN
F 1 "Conn_01x08" H 7368 3616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7450 4150 50  0001 C CNN
F 3 "~" H 7450 4150 50  0001 C CNN
	1    7450 4150
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J5
U 1 1 6030FE5E
P 7450 5550
F 0 "J5" H 7368 4925 50  0000 C CNN
F 1 "Conn_01x08" H 7368 5016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 7450 5550 50  0001 C CNN
F 3 "~" H 7450 5550 50  0001 C CNN
	1    7450 5550
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J1
U 1 1 6038448A
P 10650 5550
F 0 "J1" H 10568 4925 50  0000 C CNN
F 1 "Conn_01x08" H 10568 5016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 10650 5550 50  0001 C CNN
F 3 "~" H 10650 5550 50  0001 C CNN
	1    10650 5550
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J2
U 1 1 60384B80
P 10650 4150
F 0 "J2" H 10568 3525 50  0000 C CNN
F 1 "Conn_01x08" H 10568 3616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 10650 4150 50  0001 C CNN
F 3 "~" H 10650 4150 50  0001 C CNN
	1    10650 4150
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J3
U 1 1 60385101
P 10650 2750
F 0 "J3" H 10568 2125 50  0000 C CNN
F 1 "Conn_01x08" H 10568 2216 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 10650 2750 50  0001 C CNN
F 3 "~" H 10650 2750 50  0001 C CNN
	1    10650 2750
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J4
U 1 1 60385F84
P 10650 1350
F 0 "J4" H 10568 725 50  0000 C CNN
F 1 "Conn_01x08" H 10568 816 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 10650 1350 50  0001 C CNN
F 3 "~" H 10650 1350 50  0001 C CNN
	1    10650 1350
	1    0    0    1   
$EndComp
$Comp
L led-cube-rescue:Conn_01x08-Connector_Generic J9
U 1 1 60387CFE
P 600 5050
F 0 "J9" H 518 5567 50  0000 C CNN
F 1 "Conn_01x08" H 518 5476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 600 5050 50  0001 C CNN
F 3 "~" H 600 5050 50  0001 C CNN
	1    600  5050
	-1   0    0    -1  
$EndComp
$Comp
L led-cube-rescue:Conn_01x03-Connector_Generic J_Arduino1
U 1 1 6045ED7B
P 1800 3150
F 0 "J_Arduino1" H 1718 3467 50  0000 C CNN
F 1 "Conn_01x03" H 1718 3376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1800 3150 50  0001 C CNN
F 3 "~" H 1800 3150 50  0001 C CNN
	1    1800 3150
	-1   0    0    -1  
$EndComp
$Comp
L led-cube-rescue:Conn_02x03_Row_Letter_First-Connector_Generic J_btn1
U 1 1 60501933
P 1350 1950
F 0 "J_btn1" H 1400 2267 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_First" H 1400 2176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 1350 1950 50  0001 C CNN
F 3 "~" H 1350 1950 50  0001 C CNN
	1    1350 1950
	1    0    0    -1  
$EndComp
NoConn ~ 2500 2450
NoConn ~ 4000 6550
Wire Wire Line
	3100 2850 3100 3600
Connection ~ 3100 3600
Wire Wire Line
	3100 3600 3600 3600
NoConn ~ 3000 2850
$EndSCHEMATC
