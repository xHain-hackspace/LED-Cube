#include <Arduino.h>
#include <LEDCube.h>
// #include <Alphabet8x8.h>

#define TOTAL_EFFECTS 8
#define RAIN 0
#define PLANE_BOING 1
#define SEND_VOXELS 2
#define WOOP_WOOP 3
#define CUBE_JUMP 4
#define GLOW 5
#define TEXT 6
#define LIT 7

// text selection mode
#define NUMBERS8 0 
#define XHAIN8 1

void rain(Cube * rainCube);
void testFunct(Cube * cubePointer); // REMOVE test function

class Plane
{
public:
  static uint8_t position;
  static uint8_t direction;
  static bool looped;
  static uint8_t currentEffect;

  void init();
  void setPlane(Cube * planeCube, uint8_t axis);
  void setDirection(uint8_t axis);
  void planeBoing(Cube * planeCube);
};

class Voxels
{
public:
    static uint8_t selX;
    static uint8_t selY;
    static uint8_t selZ;
    static uint8_t sendDirection;
    static bool sending;
    static uint16_t glowCount;
    static bool glowing;

    void init();
    void sendVoxel(Cube * voxelCube); // voxel effect
    void doGlow(Cube * glowCube); // glow effect
};

class Woop
{
public:
    static uint8_t cubeSize;
    static bool cubeExpanding;
    static uint8_t xPos;
    static uint8_t yPos;
    static uint8_t zPos;

    // woop woop cube
    void init();
    void drawCube(Cube * woopCube, uint8_t x, uint8_t y, uint8_t z, uint8_t s);
    void doWoop(Cube * woopCube);

    // jumping cube
    void doJump(Cube * jumpCube);
};

class Text
{
public:
    static uint8_t charCounter;
    static uint8_t charPosition;
    static uint8_t textxhain[5][8];

    void init(Cube * textCube);
    void showText(Cube * textCube, int mode);
};